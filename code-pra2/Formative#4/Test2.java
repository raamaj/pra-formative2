public class Test2 { // Inisialisasi class Test2 dengan modifier public (bisa diakses dimana saja)
	int a = 1; // Inisialisasi variable instance a bertipe data integer yang diberi nilai 1
	int b = 2; // Inisialisasi variable instance b bertipe data integer yang diberi nilai 2

	public Test2 func(Test2 obj) // Method dengan nama func bertipe data Test2 (class) dengan parameter obj bertipe data Test2 (class)
	{
		Test2 obj3 = new Test2(); // Instansiasi obj3 sebagai objek Test2 baru
		obj3 = obj; // Memberi nilai obj3 dengan nilai dari parameter obj
		obj3.a = obj.a++ + ++obj.b; // variable a merupakan penjumlahan dari increment obj.a (postfix a) ditamban dengan increment obj.b (prefix)
		obj.b = obj.b; // memberi nilai variable b pada obj dengan nilai variable b pada obj.
		return obj3; // mengembalikan nilai obj3
	}

	public static void main(String[] args) // Main method pada java (modifier public, tipe static yang tak bisa diubah, tidak memerlukan return
	{
		Test2 obj1 = new Test2(); // Instansiasi obj1 sebagai objek Test2 baru
		Test2 obj2 = obj1.func(obj1); // Instansiasi obj2 sebagai objek Test2 dengan nilai dari method func() pada obj1

		System.out.println("obj1.a = " + obj1.a + " obj1.b = " + obj1.b); // Menampilkan variabel a dan b pada obj1
		System.out.println("obj2.a = " + obj2.a + " obj1.b = " + obj2.b); // Menampilkan variable a dan b pada obj2
    		System.out.println("obj2 = " + obj2); // Menampilkan objek obj2 dan address memory nya
    		System.out.println("obj1 = " + obj1); // Menampilkan objek obj1 dan address memory nya

	}
}

/*

Ketika program dijalankan, program akan membuat obj1 sebagai objek baru dari Test2.
Kemudian membuat obj2 dengan nilai dari penghitungan method func pada obj1 yang memasukkan nilai obj1 sebagai parameternya (nilai variable a = 1 dan b = 2).
Di dalam method func pada obj1, obj3 dibuat sebagai objek baru dari Test2 kemudian nilainya diisi dengan obj merupakan parameternya.
nilai dari obj3 yaitu variable a = 1 dan variable b = 2.

Kemudian nilai dari variable a pada obj3 dihitung sebagai increment (postfix) variable a pada obj 
yang nilainya 1 (karena untuk postfix nilainya disimpan dulu kemudian di increment), ditambah dengan
increment (prefix) variable b pada obj yang nilainya 3 (karena untuk prefix, di increment dulu baru disimpan -> b = 2 + 1 = 3).
Sehingga nilai variable a untuk obj3 adalah 4 ( 1 + 3 ).

Kemudian nilai dari variable b pada obj di asign kembali dengan variable b pada obj ( 3 = 3 )
Terakhir, nilai dari obj3 di return yang akan menghasilkan nilai variable a = 4 dan b = 3.

Sehingga, nilai pada obj2 juga akan memengaruhi nilai dari obj1 yang menghasilkan nilai yang sama.
Ketika objek ditampilkan pada layar, menghasilkan alamat memori yang sama. Hal ini menunjukkan bahwa
obj1 dan obj2 adalah objek yang sama.

*/