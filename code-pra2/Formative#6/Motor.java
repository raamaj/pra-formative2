class Motor extends SuperClass {
	Motor(String name, String type, double price){
		this.name = name;
		this.type = type;
		this.price = price;
	}

	public int wheel(){
		return 2;
	}

	public boolean engine(){
		return true;
	}

	public String getName(){
		return this.name;
	}

	public String getType(){
		return this.type;
	}

	public double getPrice(){
		return this.price;
	}
}