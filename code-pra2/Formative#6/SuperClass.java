public abstract class SuperClass {
	public String name;
	public String type;
	public double price;
	
	public abstract int wheel();
	public abstract boolean engine();
}