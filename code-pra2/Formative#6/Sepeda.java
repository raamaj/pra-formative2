public class Sepeda extends SuperClass {
	Sepeda(String name, String type, double price){
		this.name = name;
		this.type = type;
		this.price = price;
	}

	public int wheel(){
		return 2;
	}

	public boolean engine(){
		return false;
	}

	public String getName(){
		return this.name;
	}

	public String getType(){
		return this.type;
	}

	public double getPrice(){
		return this.price;
	}
}