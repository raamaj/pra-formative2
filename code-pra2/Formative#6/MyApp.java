public class MyApp{
	public static void main(String[] args){
		Sepeda sepeda = new Sepeda("Polygon M20", "MTB", 2500000d);
		Motor motor = new Motor("TDR 5000", "Sport", 50000000d);
		
		System.out.println("Objek Sepeda : ");
		System.out.println("Name : " + sepeda.getName());
		System.out.println("Type : " + sepeda.getType());
		System.out.println("Price : " + sepeda.getPrice());
		System.out.println("Num of Wheel : " + sepeda.wheel());
		System.out.println("Has Engine : " + sepeda.engine());
		System.out.println("=============================================");
		System.out.println("Objek Motor: ");
		System.out.println("Name : " + motor.getName());
		System.out.println("Type : " + motor.getType());
		System.out.println("Price : " + motor.getPrice());
		System.out.println("Num of Wheel : " + motor.wheel());
		System.out.println("Has Engine : " + motor.engine());
	}
}