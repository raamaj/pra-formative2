public class Test {
    public static void main(String[] args) {
        Test obj = new Test();
        obj.start();
    }
    public void start() {
        String stra = "do";
        String strb = method(stra);
        System.out.print(": "+stra + strb);
    }
    public String method(String stra) {
        stra = stra + "good";
        System.out.print(stra);
        return " good";
    }
}

/*

Baris kode "public class Test" merupakan baris untuk mendefinisikan sebuah kelas
yang bernama Test dengan modifier public yang berarti kelas dapat dipanggil dimana saja.
Class body atau blok kelas berada diantara tanda {}.

Baris kode "public static void main(String[] args)" merupakan main method pada java dengan modifier public
yang berarti method dapat dipanggil dimana saja. Pada method ini juga terdapat keyword static yang artinya
method ini tidak dapat diubah serta kata kunci void yang berarti pada method ini tidak perlu ada return value.
Method ini juga terdapat parameter array String dengan nama args.

Blok pada main method terdapat sebuah instansiasi kelas dengan kode "Test obj = new Test()" yang artinya membuat
instance kelas Test dengan nama obj. Pada blok ini juga terdapat kode untuk memanggil method start() yang ada
pada kelas Test (obj.start()).

Method start() merupakan method dengan modifier public dan tipe return void yang tidak memerlukan return value.
Pada method ini diinisialisasikan variabel stra dengan tipe data String dan diberi nilai "do", serta tipe data strb
dengan tipe data String dan diberi nilai method(stra) yaitu memanggil method "method()" dengan mengirimkan parameter stra.
Pada method ini juga terdapat perintah untuk menampilkan hasil dari stra dan strb.

Method "method()" merupakan method dengan modifier public dan tipe return String yang artinya mengembalikan nilai dengan tipe String.
Pada method ini terdapat parameter stra dengan tipe data String. Pada method body terdapat perintah untuk melakukan concatination terhadap
string, yaitu variabel stra ditambah dengan string "good". Hasil dari proses tersebut kemudian ditampilkan ke layar, kemudian method ini
mengembalikan nilai " good".

Sehingga hasil dari program ini akan menampilkan : "dogood: do good".

*/
