class Test5 {
public static void main(String[] args)
    {
        int arr[] = { 1, 2, 3 };

        // final with for-each statement
        for (final int i : arr)
            System.out.print(i + " ");
    }
}

/*

Baris kode "class Test5" merupakan insialisasi kelas dengan nama Test5.
Meskipun pada kelas ini tidak dituliskan modifiernya, kelas ini secara bawaan
memiliki modifier "default" yang mana kelas ini dapat dipanggil pada kelas ini sendiri atau
pada kelas lain di dalam package yang sama.

Di dalam blok kelas ini terdapat "public static void main(String[] args)" yang merupakan
method main (bawaan) dari java yang memiliki modifier public, bertipe static (tidak dapat diubah),
tidak memiliki return value (void), serta memiliki parameter array String dengan nama args.

Di dalam blok method main terdapat inisialisai array dengan tipe data integer dengan nama arr 
yang diberi nilai 1 (nilai index ke 0), 2 (nilai index ke 1), dan 3 (nilai index ke 2).

di dalam method main juga terdapat blok for loop untuk menampilkan elemen dari variable arr. 
Pada kondisi for ini diberi kata kunci final untuk variable i yang berarti bahwa tidak megijinkan adanya
perubahan terhadap variable i pada blok for selama pengulangan. nilai dari variabel i akan diisi dengan
tiap elemen pada variable arr untuk setiap pengulangannya dan ditampilkan ke layar.

Senhingga, hasil dari program ini adalah menampilkan : 1 2 3

*/